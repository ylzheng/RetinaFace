import cv2
import numpy as np

from detector import RetinaFaceDetector

if __name__ == "__main__":
    detector = RetinaFaceDetector('weights/mobilenet0.25_Final.pth')
    img = cv2.imread("examples/trump.jpg")
    img2 = cv2.imread("examples/trump.jpg")
    input_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    input_img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
    faces = detector.detect([input_img, input_img2])
    box, landmarks, score = faces[1][0]
    box = box.astype(np.int)
    cropped = img2[box[1]:box[3], box[0]:box[2]]
    cv2.imshow("test", cropped)
    cv2.waitKey(0)
